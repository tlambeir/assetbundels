﻿using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
public class CreateAssetBundles
{
	static void addDependenciesFromPrefab(string pathToPrfab, List<string> assets){
		Debug.Log("looking for dependencies for:" + pathToPrfab);
		string[] dependencies = AssetDatabase.GetDependencies(pathToPrfab);
		for (int j = 0; j < dependencies.Length; j++) {
			if (Path.GetExtension (dependencies [j]) != ".cs") {
				assets.Add(dependencies[j]);
				Debug.Log("Added:" + dependencies[j]);
			}
		}
	}

    [MenuItem("Assets/Build Scene Bundle")]
    static void BuildSceneBundle()
    {
        List<string> assets = new List<string>();

		string currentScene = SceneManager.GetActiveScene().name;

		// Add prefabs
		string basePrefabPath = "Assets/Prefabs/";

		// find and add dependecies from prefab
		addDependenciesFromPrefab(basePrefabPath + currentScene + ".prefab", assets);

		// add Sprites
		string baseStringPath = "Assets/Resources/Sprites/";
		addDependenciesFromPrefab(baseStringPath + currentScene, assets);

        //add multimedia content
        string baseMediaPath = "Assets/Multimedia/";

        addDependenciesFromPrefab(baseMediaPath + "Videos", assets);
        addDependenciesFromPrefab(baseMediaPath + "Images", assets);

        //Add to an assetbundle build
        AssetBundleBuild[] abb = new AssetBundleBuild[1];
        abb[0].assetNames = assets.ToArray();
		abb[0].assetBundleName = "bundle_" + currentScene;

        //Build the asset bundle
		BuildPipeline.BuildAssetBundles("Assets/AssetBundles", abb, BuildAssetBundleOptions.ForceRebuildAssetBundle, BuildTarget.iOS);
    }
}