﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace maxstAR
{
	[CustomEditor(typeof(MapViewerBehaviour))]
	public class MapViewerEditor : Editor
	{
		private MapViewerBehaviour mapController = null;

		private bool changedSlideBar = false;
		private bool changedCheckBox = false;
		private bool autoCamera = false;
		private bool transparent = false;
		private bool reconstruction = false;
		private int keyframeId = 0;

		public void OnEnable()
		{
			if (PrefabUtility.GetPrefabType(target) == PrefabType.Prefab)
			{
				return;
			}
		}

		public override void OnInspectorGUI()
		{
			if (PrefabUtility.GetPrefabType(target) == PrefabType.Prefab)
			{
				return;
			}

			mapController = (MapViewerBehaviour)target;

			changedSlideBar = false;
			changedCheckBox = false;

			keyframeId = EditorGUILayout.IntSlider("Keyframe Id", mapController.ImageIndex, 0, mapController.ImageCount - 1);
			reconstruction = EditorGUILayout.Toggle("Generate Mesh", mapController.Reconstruction);
			autoCamera = EditorGUILayout.Toggle("Auto Camera", mapController.AutoCamera);
			transparent = EditorGUILayout.Toggle("Transparent", mapController.Transparent);

			if (mapController.ImageIndex != keyframeId)
			{
				changedSlideBar = true;
				mapController.ImageIndex = keyframeId;
			}

			if (mapController.Reconstruction != reconstruction)
			{
				changedCheckBox = true;
				mapController.Reconstruction = reconstruction;
			}

			if (mapController.AutoCamera != autoCamera)
			{
				changedCheckBox = true;
				mapController.AutoCamera = autoCamera;
			}

			if (mapController.Transparent != transparent)
			{
				changedCheckBox = true;
				mapController.Transparent = transparent;
			}

			serializedObject.Update();

			if (GUI.changed)
			{
				if (mapController != null)
				{
					if (changedSlideBar || changedCheckBox)
					{
						mapController.UpdateMapViewer();
					}

					EditorUtility.SetDirty(mapController);
				}
			}
		}

		void OnSceneGUI()
		{
			if (Event.current.GetTypeForControl(GUIUtility.GetControlID(FocusType.Passive)) == EventType.ScrollWheel)
			{
				if (autoCamera)
				{
					Vector3 position = SceneView.lastActiveSceneView.camera.transform.position;
					Quaternion rotation = SceneView.lastActiveSceneView.camera.transform.rotation;
					mapController.ApplyViewCamera(position, rotation);
					mapController.UpdateMapViewer();
				}
			}
		}
	}
}