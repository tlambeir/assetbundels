﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightmapParam : MonoBehaviour {
	public Texture2D lightmapNear, lightmapFar; 
	public int lightmapIndex;
    public Vector4 lightmapScaleOffset;
}
