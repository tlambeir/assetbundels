﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Launch_Animation : MonoBehaviour {

	private Animator animator;
	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator> ();
	}

	public void triggerAnimation(string name = "Anim_State_1_Trigger"){
		animator.SetTrigger (name);
	}

	public void setIdle(){
		animator.ResetTrigger ("Anim_State_3_Trigger");
	}
}
