﻿using UnityEngine;
using System.Collections.Generic;
using System.Text;

using maxstAR;

public class CameraConfigurationSample : MonoBehaviour
{
	private Dictionary<string, ImageTrackableBehaviour> imageTrackablesMap =
		new Dictionary<string, ImageTrackableBehaviour>();
	private bool startTrackerDone = false;
	private bool cameraStartDone = false;

	void Start()
	{
		imageTrackablesMap.Clear();
		ImageTrackableBehaviour[] imageTrackables = FindObjectsOfType<ImageTrackableBehaviour>();
		foreach (var trackable in imageTrackables)
		{
			imageTrackablesMap.Add(trackable.TrackableName, trackable);
			Debug.Log("Trackable add: " + trackable.TrackableName);
		}

		AddTrackerData();
	}

	private void AddTrackerData()
	{
		foreach (var trackable in imageTrackablesMap)
		{
			if (trackable.Value.TrackerDataFileName.Length == 0)
			{
				continue;
			}

			if (trackable.Value.StorageType == StorageType.AbsolutePath)
			{
				TrackerManager.GetInstance().AddTrackerData(trackable.Value.TrackerDataFileName);
			}
			else
			{
				if (Application.platform == RuntimePlatform.Android)
				{
					TrackerManager.GetInstance().AddTrackerData(trackable.Value.TrackerDataFileName, true);
				}
				else
				{
					TrackerManager.GetInstance().AddTrackerData(Application.streamingAssetsPath + "/" + trackable.Value.TrackerDataFileName);
				}
			}
		}

		TrackerManager.GetInstance().LoadTrackerData();
	}

	private void DisableAllTrackables()
	{
		foreach (var trackable in imageTrackablesMap)
		{
			trackable.Value.OnTrackFail();
		}
	}

	void Update()
	{
		if (Input.GetKey(KeyCode.Escape))
		{
			SceneStackManager.Instance.LoadPrevious();
		}

		StartCamera();

		if (!startTrackerDone)
		{
			TrackerManager.GetInstance().StartTracker(TrackerManager.TRACKER_TYPE_IMAGE);
			startTrackerDone = true;
		}

		DisableAllTrackables();

		TrackingResult trackingResult = TrackerManager.GetInstance().GetTrackingResult();

		for (int i = 0; i < trackingResult.GetCount(); i++)
		{
			Trackable trackable = trackingResult.GetTrackable(i);
			imageTrackablesMap[trackable.GetName()].OnTrackSuccess(
				trackable.GetId(), trackable.GetName(), trackable.GetPose());
		}

		// For Image extractor test
		maxstAR.Image image = ImageExtractor.GetInstance().GetImage();
		if (image != null)
		{
			int width = image.GetWidth();
			int height = image.GetHeight();
			byte [] data = image.GetData();

			Debug.Log("width : " + width);
			Debug.Log("height : " + height);
			Debug.Log("length : " + image.GetLength());
			Debug.Log("format : " + image.GetFormat());
			Debug.Log("data : " + data[0] + ", " + data[1] + ", " + data[2] + ", " + data[3]);
		}
	}

	bool flashOn = false;
	bool horizontalFlip = false;
	bool verticalFlip = false;
	bool focusContinuous = true;
	bool whiteBalanceLock = false;

	public void SetFlashLightMode()
	{
		flashOn = !flashOn;
		CameraDevice.GetInstance().SetFlashLightMode(flashOn);
	}

	public void FlipHorizontal()
	{
		horizontalFlip = !horizontalFlip;
		CameraDevice.GetInstance().FlipVideo(CameraDevice.FlipDirection.HORIZONTAL, horizontalFlip);
	}

	public void FlipVertical()
	{
		verticalFlip = !verticalFlip;
		CameraDevice.GetInstance().FlipVideo(CameraDevice.FlipDirection.VERTICAL, verticalFlip);
	}

	public void WhiteBalanceLock()
	{
		whiteBalanceLock = !whiteBalanceLock;
		CameraDevice.GetInstance().SetAutoWhiteBalanceLock(whiteBalanceLock);
	}

	public void SetContinuousFocus()
	{
		focusContinuous = !focusContinuous;
		if (focusContinuous)
		{
			CameraDevice.GetInstance().SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUS_AUTO);
		}
		else
		{
			CameraDevice.GetInstance().SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_AUTO);
		}
		
	}

	void OnApplicationPause(bool pause)
	{
		if (pause)
		{
			TrackerManager.GetInstance().StopTracker();
			startTrackerDone = false;
			StopCamera();
		}
	}

	void OnDestroy()
	{
		CameraDevice.GetInstance().SetFlashLightMode(false);

		imageTrackablesMap.Clear();
		TrackerManager.GetInstance().SetTrackingOption(TrackerManager.TrackingOption.NORMAL_MODE);
		TrackerManager.GetInstance().StopTracker();
		TrackerManager.GetInstance().DestroyTracker();
		StopCamera();
	}

	void StartCamera()
	{
		if (!cameraStartDone)
		{
			Debug.Log("Unity StartCamera");
			ResultCode result = CameraDevice.GetInstance().Start();
			if (result == ResultCode.Success)
			{
				cameraStartDone = true;
				CameraDevice.GetInstance().SetAutoWhiteBalanceLock(true);  // For ODG-R7 preventing camera flickering
			}
		}
	}

	void StopCamera()
	{
		if (cameraStartDone)
		{
			Debug.Log("Unity StopCamera");
			CameraDevice.GetInstance().Stop();
			cameraStartDone = false;
		}
	}
}